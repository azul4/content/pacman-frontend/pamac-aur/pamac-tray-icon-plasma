# pamac-tray-icon-plasma

Pamac tray icon for plasma users

https://gitlab.com/LordTermor/pamac-tray-icon-plasma

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/pacman-frontend/pamac-aur/pamac-tray-icon-plasma.git
```

